# Parser of the template file/ in pgen application.
# Copyright (C) 2018  Anonymous

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os


class TemplateParser(object):
    def __init__(self, path):
        try:
            self.content = ""
            if os.path.isdir(path):
                self.addContentDir(path)
            elif os.path.exists(path):
                self.addContent(path)
            else:
                raise RuntimeError("Error: no template file is found")
        except Exception:
            raise RuntimeError("Error: failed to open the template file(s). " +
                               "The path could be wrong.")

    def replaceWord(self, old, new):
        self.content = self.content.replace(old, new)

    def getContent(self, identifier):
        begin = self.content.find(identifier) + len(identifier)
        end = self.content.find(identifier, begin)
        if begin > -1 and end > -1:
            return self.content[begin:end]

    def addContent(self, path):
        with open(path, 'r') as f:
            self.content += f.read()
        f.close()

    def addContentDir(self, path):
        for root, directories, filenames in os.walk(path):
            for filename in filenames:
                self.addContent(os.path.join(root, filename))
