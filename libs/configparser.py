# Parser of the config file in pgen application.
# Copyright (C) 2018  Anonymous

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function
import toml
import string


class ConfigParser(object):
    __variables = {"PREFIX": "#m@",
                   "SUFFIX": "@m#",
                   "PROJECT_ID": "++PROJECT_NAME++"}
    __root = {}

    def __init__(self, path):
        self.parsed = toml.load(path)
        if "IDENTIFIERS" in self.parsed.keys():
            for key in self.__variables.keys():
                if key in self.parsed["IDENTIFIERS"]:
                    self.__variables[key] = self.parsed["IDENTIFIERS"][key]
        if "STRUCTURE" not in self.parsed.keys():
            raise RuntimeError("Config file is invalid")

    def parseStructure(self, name):
        self.parseRecursive(self.parsed["STRUCTURE"], name, ".", name)

    def parseRecursive(self, parsing, currentdir, path, pname):
        self.nameValidation(currentdir)
        self.__root[path + "/" + currentdir] = []

        for key, value in parsing.items():
            if key == "FILES":
                self.parseFiles(value, self.__root[path + "/" + currentdir],
                                currentdir)
            elif key == "CREATE_MAIN" and currentdir == "src":
                self.__root[path + "/" + currentdir].append(
                    [pname + ".cpp",
                     self.__variables["PREFIX"] +
                     "src#" + pname + ".cpp" +
                     self.__variables["SUFFIX"]])
            elif key == "CREATE_MAIN" and currentdir == "test":
                self.__root[path + "/" + currentdir].append(
                    ["test" + pname + ".cpp",
                     self.__variables["PREFIX"] +
                     "test#test" + pname + ".cpp" +
                     self.__variables["SUFFIX"]])
            elif isinstance(value, dict):
                self.parseRecursive(value, key, path + "/" + currentdir, pname)

    def parseFiles(self, parsing, dest, dirname):
        for elm in parsing:
            self.nameValidation(elm[0])
            if len(elm) == 1:
                dest.append([elm[0], self.variable["PREFIX"] + dirname + "#" +
                             elm[0] + self.__variables["SUFFIX"]])
            elif len(elm) == 2:
                dest.append([elm[0], self.__variables["PREFIX"] + elm[1] +
                             self.__variables["SUFFIX"]])
            else:
                raise RuntimeError("Invalid file value")

    def printStructure(self):
        for k, v in self.__root.items():
            print(k)
            for el in v:
                print(el, end=" ")
            print("\n")

    def getStructure(self):
        return dict(self.__root)

    def getProjectId(self):
        return self.__variables["PROJECT_ID"]

    def nameValidation(self, name):
        if not name:
            raise RuntimeError("An empty file name is invalid")
        if name[0] == '.' and (len(name) == 1 or name[1] == '.'):
            raise RuntimeError("'" + name + "' is and invalid name")

        valid = "._-%s%s" % (string.ascii_letters, string.digits)
        for c in name:
            if c not in valid:
                raise RuntimeError("'" + name + "' has invalid characters")
