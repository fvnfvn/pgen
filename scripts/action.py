# This scripts install and uninstall the pgne program on a linux machien.
# Copyright (C) 2018  Anonymous

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import os
import shutil

SOURCE_PATH = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
LIB_ELEMENTS = ['pgen', 'libs']
LIB_DESTINATION = os.path.expanduser("~") + "/.bin/pgen"
DEFAULT_ARGS = "inputfiles"
ARGS_DESTINATION = os.path.expanduser("~") + "/.pgen"


def isTomlInstalled():
    try:
        import toml
        return True
    except Exception:
        return False


def libExists():
    return os.path.isdir(LIB_DESTINATION) or os.path.exists(LIB_DESTINATION)


def inputFileExist():
    return os.path.exists(ARGS_DESTINATION)


def changeMod(name):
    if sys.version_info[0] < 3:
        os.chmod(name, 0755)
    else:
        os.chmod(name, 0o755)


def uninstall():
    uninstalled = False
    if inputFileExist():
        shutil.rmtree(ARGS_DESTINATION, ignore_errors=True)
        uninstalled = True
    if libExists():
        shutil.rmtree(LIB_DESTINATION, ignore_errors=True)
        uninstalled = True

    if uninstalled:
        print("Uninstallation is successfully finished.")
        print("Please remove " + LIB_DESTINATION + " from your system path")
    else:
        print("Error: failed to uninstall the application. The application\n" +
              "       might not be installed.")
        sys.exit(1)


def install():
    if not isTomlInstalled():
        print("Module 'toml' is required and it seems to to be installed.\n" +
              "Please install the module and try the installain again.")
        sys.exit(1)

    if inputFileExist() or libExists():
        print("Error: the application might be already installed or\n " +
              "      the previous installation might be corrupted. Please\n"
              "       execute uninstall command and try the installation agin")
        sys.exit(1)

    try:
        os.makedirs(LIB_DESTINATION)

        shutil.copyfile(SOURCE_PATH + "/" + LIB_ELEMENTS[0], LIB_DESTINATION +
                        "/" + LIB_ELEMENTS[0])
        changeMod(LIB_DESTINATION + "/" + LIB_ELEMENTS[0])

        shutil.copytree(SOURCE_PATH + "/" + LIB_ELEMENTS[1],
                        LIB_DESTINATION + "/" + LIB_ELEMENTS[1])

        shutil.copytree(SOURCE_PATH + "/" + DEFAULT_ARGS, ARGS_DESTINATION)

        print("Installation is successfully finished.")
        print("Please add " + LIB_DESTINATION + " to your system path")
        sys.exit(0)

    except Exception:
        shutil.rmtree(LIB_DESTINATION, ignore_errors=True)
        shutil.rmtree(ARGS_DESTINATION, ignore_errors=True)
        print("Error: failed to install!")
        sys.exit(1)


def main():
    if len(sys.argv) != 2 or (sys.argv[1] != "install" and
                              sys.argv[1] != "uninstall"):
        print("USAGE:\n\t" + sys.argv[0] + " installl | uninstall")
        return
    else:
        if sys.argv[1] == "install":
            install()
        else:
            uninstall()


if __name__ == "__main__":
    main()
