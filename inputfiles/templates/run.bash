#m@rbash@m#
#!/bin/bash

#################################### USAGE #####################################
#                                                                              #
# This script executes build and test commands for the current project         #
#                                                                              #
################################################################################

################################## IMPORTANT ###################################
#                                                                              #
# To be able to use this script, the script and functions.sh should be placed  #
# in the same direcotry. The directoy containing them should be a subdirectory #
# of the root direcory. The structure is like:                                 #
# ROOT                                                                         #
# |__scripts                                                                   #
#     |__run.bash                                                              #
#     |__functions.sh                                                          #
#                                                                              #
################################################################################

SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
PATH_ROOT=${SCRIPT_PATH%/*}
COMMANDLINE_ARGUMENTS=('help' 'debug' 'release' 'test')
LIB="functions.bash"
DIR_NAME=$(dirname $0)

function join_by { local d=$1; shift; echo -n "$1"; shift; printf "%s" "${@/#/$d}"; }
function show_help() {
    echo "A script to execute usefull commands."
    echo -e "Usage:\t$(basename "$0") [$(join_by ' | ' ${COMMANDLINE_ARGUMENTS[@]})]"
}

source $DIR_NAME/$LIB

if [ $# -eq 0 ]
then
    PATH_DESTINATION="$PATH_ROOT/build/debug"
    build_debug $PATH_DESTINATION
    exit 0
fi


case "$1" in
    debug)
	PATH_DESTINATION="$PATH_ROOT/build/debug"
	build_debug $PATH_DESTINATION
	exit 0;;
    release)
	PATH_DESTINATION="$PATH_ROOT/build/release"
	build_release $PATH_DESTINATION
	exit 0;;
    test)
	PATH_DESTINATION="$PATH_ROOT/build/debug/test"
	execute_tests $PATH_DESTINATION
	exit 0;;
    *)
	show_help
	exit 0;;
esac
#m@rbash@m#
